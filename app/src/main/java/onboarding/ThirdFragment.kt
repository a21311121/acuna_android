package onboarding

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager2.widget.ViewPager2
import mx.edu.uthermosillo.a21311121.eventos.R
import mx.edu.uthermosillo.a21311121.eventos.databinding.FragmentFirstBinding
import mx.edu.uthermosillo.a21311121.eventos.databinding.FragmentThirdBinding


class ThirdFragment : Fragment() {
    private var _binding : FragmentThirdBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view:View=inflater.inflate(R.layout.fragment_first, container, false)

        val viewPager2 = activity?.findViewById<ViewPager2>(R.id.onBoardingViewPager)
        binding.botonThird.setOnClickListener {
            viewPager2?.currentItem = 3
        }

        return view
    }

}
