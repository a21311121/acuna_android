package mx.edu.uthermosillo.a21311121.eventos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mx.edu.uthermosillo.a21311121.eventos.ui.adapters.OnboardingAdapter
import mx.edu.uthermosillo.a21311121.eventos.databinding.FragmentInitBinding
import onboarding.FirstFragment
import onboarding.SecondFragment
import onboarding.ThirdFragment

class InitFragment : Fragment() {
    private var _binding : FragmentInitBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentInitBinding.inflate(inflater, container, false)

        val viewPager2 = binding.onBoardingViewPager

        val fragmentList = arrayListOf<Fragment>(
            FirstFragment(),
            SecondFragment(),
            ThirdFragment(),
            LoginFragment()
        )

        val adapter = OnboardingAdapter (fragmentList,
            requireActivity().supportFragmentManager, lifecycle)
        viewPager2.adapter = adapter

        return binding.root
    }
}