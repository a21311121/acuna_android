package mx.edu.uthermosillo.a21311121.eventos.data.models

data class City(
    val name: String,
    val state: State,
    val isCapital: Boolean,
    val id: Int

)
