package mx.edu.uthermosillo.a21311121.eventos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import mx.edu.uthermosillo.a21311121.eventos.data.models.itemdashboard
import mx.edu.uthermosillo.a21311121.eventos.databinding.FragmentDashboardBinding
import mx.edu.uthermosillo.a21311121.eventos.ui.adapters.DashboardAdapter

class DashboardFragment : Fragment() {

    private var _binding : FragmentDashboardBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        // Configurar RecyclerView
        val items = listOf(
            itemdashboard("Compras", R.drawable.moneda),
            itemdashboard("Saldo", R.drawable.moneda),
            itemdashboard("Pago", R.drawable.moneda)
            // Agrega más ítems según necesites
        )

        val adapter = DashboardAdapter(items)
        binding.recyclerView2.layoutManager = LinearLayoutManager(context)
        binding.recyclerView2.adapter = adapter

        // Obtener datos del usuario de Firebase
        val user = FirebaseAuth.getInstance().currentUser
        val userName = user?.displayName ?: "Nombre"
        val userEmail = user?.email ?: "Correo electrónico"
        val userPhoto = user?.photoUrl?.toString()

        // Mostrar datos en el dashboard
        binding.profileName.text = userName
        binding.profileEmail.text = userEmail
        if (!userPhoto.isNullOrEmpty()) {
            Glide.with(this)
                .load(userPhoto)
                .into(binding.profileImage)
        } else {
            binding.profileImage.setImageResource(R.drawable.ic_launcher_foreground)
        }

        // Configurar el botón de logout
        binding.buttonLogout.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            Toast.makeText(requireContext(), "Sesión cerrada", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_dashboardFragment_to_firstFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
