package mx.edu.uthermosillo.a21311121.eventos

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import mx.edu.uthermosillo.a21311121.eventos.databinding.FragmentHistoryBinding
import mx.edu.uthermosillo.a21311121.eventos.databinding.FragmentListeventsfragmentBinding


class ListeventsFragment : Fragment() {

    private var _binding : FragmentListeventsfragmentBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListeventsfragmentBinding.inflate(inflater, container, false)

        return binding.root
    }


}