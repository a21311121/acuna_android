package mx.edu.uthermosillo.a21311121.eventos.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import mx.edu.uthermosillo.a21311121.eventos.data.models.itemdashboard
import mx.edu.uthermosillo.a21311121.eventos.databinding.ItemDashboardBinding

class DashboardAdapter(private val items: List<itemdashboard>) :
    RecyclerView.Adapter<DashboardAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemDashboardBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: itemdashboard) {
            binding.name.text = item.name
            Glide.with(binding.image.context).load(item.imageURL).into(binding.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemDashboardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size
}