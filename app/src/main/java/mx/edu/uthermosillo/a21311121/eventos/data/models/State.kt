package mx.edu.uthermosillo.a21311121.eventos.data.models

data class State(
    val name: String,
    val country: Country,
    val id: Int
)
