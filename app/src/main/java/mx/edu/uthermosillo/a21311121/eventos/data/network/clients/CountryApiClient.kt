package mx.edu.uthermosillo.a21311121.eventos.data.network.clients

import retrofit2.Response
import mx.edu.uthermosillo.a21311121.eventos.data.models.Country
import retrofit2.http.GET
import retrofit2.http.Path

interface CountryApiClient {

    @GET("countries")
    suspend fun getCountries(): Response<List<Country>>

    @GET("countries/{id}")
    suspend fun getCountry(@Path("id") id :Int): Response<Country>


}