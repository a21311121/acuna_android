package mx.edu.uthermosillo.a21311121.eventos.data.models

data class Country(
    val name: String,
    val continent: String,
    val population: Int,
    val language: String,
    val currency: String,
    val id: Int
)
